/*
 * kmp_barrier_vec.cpp
 */

//===----------------------------------------------------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//


// ---------------------------- Barrier Algorithms ----------------------------




// End of Barrier Algorithms

/////////////
// Added the following vector 2D array to team : kmp_balign_team_t ; //(kmp_barrier_team_union)
// This will be used by all threads for barrier sync 
// kmp_uint8 ** b_arrived_vflags; // team->t.t_bar[bt].b_arrived_vflags[l][t]
// kmp_uint padding, levels;
// Allocate and initialize it in __kmp_allocate_team_arrays
// Reallocate it in __kmp_reallocate_team_arrays
// Free it in __kmp_free_team_arrays
// extern kmp_uint32 __kmp_barrier_vector_padding defined in kmp.h, initialized in kmp_runtime.cpp
// __kmp_stg_parse_barrier_vector_padding, __kmp_stg_print_barrier_vector_padding added to kmp_settings.cpp for reading __kmp_barrier_vector_padding from ENV
// and added corresponding setting line in the __kmp_stg_table[]  
// static void __kmp_set_team_vector_array(kmp_team_t *team) added in kmp_runtime.cpp to set bytes in vector array for team
// Called __kmp_set_team_vector_array inside __kmp_allocate_team in kmp_runtime.cpp two times.
// Added barrier pattern bp_hyper_vector_bar to the enum and set it as default and added case to __kmp_barrier_templete to call the gather/release functions
////////////